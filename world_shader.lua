return [[
extern Image materials;
extern number material_size;
extern number world_size;

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{
  vec4 texturecolor = Texel(texture, texture_coords);
  number material = floor(texturecolor.r * 16) - 1;
  if(material > 0){
    vec2 material_coords;
    texture_coords = mod(texture_coords, material_size/world_size)*material_size*0.25;
    material_coords.x = mod(material, 4)*0.25 + texture_coords.x;
    material_coords.y = floor(material / 4)*0.25 + texture_coords.y;
    vec4 materialcolor = Texel(materials, material_coords);
    return materialcolor;
  } else{
    return vec4(0, 0, 0, 0);
  }
}
]]
