function love.conf(t)
  t.version = "0.10.1"

  t.window.width = 1280
  t.window.height = 720

  t.modules.physics = false
  t.modules.video = false
end
