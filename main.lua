local Camera = require "lib.hump.camera"

local Player = require "player"
local World = require "world"

function love.load()
  camera = Camera.new(128, 128, 3)
  world = World("art/Untitled.png")
  player = Player(10, 0, "art/sakamoto.png")
  goomba = Player(100, 150, "art/goomba.png")
end

function love.update(dt)
  player:control(dt, world)
  player:update(dt)

  goomba:auto_wander(dt, world)
  goomba:update(dt)

  if player:is_touching(goomba) then
    player:take_damage(goomba)
  end

  -- draw
  local drawX, drawY = camera:mousepos()
  drawX = math.floor(drawX)
  drawY = math.floor(drawY)
  if love.mouse.isDown(1) then
    for x = drawX-2, drawX+2 do
      if x >= 0 and x < world.bgData:getWidth() then
        for y = drawY-2, drawY+2 do
          if y >= 0 and y < world.bgData:getHeight() then
            r, g, b, a = world.bgData:getPixel(x, y)
            world.bgData:setPixel(x, y, 80, g, b, a)
          end
        end
      end
    end
    world.bg:refresh()
  elseif love.mouse.isDown(2) then
    for x = drawX-2, drawX+2 do
      if x >= 0 and x < world.bgData:getWidth() then
        for y = drawY-2, drawY+2 do
          if y >= 0 and y < world.bgData:getHeight() then
            r, g, b, a = world.bgData:getPixel(x, y)
            world.bgData:setPixel(x, y, 0, g, b, a)
          end
        end
      end
    end
    world.bg:refresh()
  end
end

function love.draw()
  love.graphics.setBackgroundColor(255, 255, 255)

  camera:draw(function()
    world:draw()
    player:draw()
    goomba:draw()
  end)

  -- cursor
  love.graphics.setColor(255, 0, 0)
  love.graphics.rectangle("line", love.mouse.getX()-4, love.mouse.getY()-4, 8, 8)
end

function love.keypressed(key)
  if key == "escape" then
    love.event.push("quit")
  end
end
