bitmaps = $(wildcard art/*.png)
assets = $(bitmaps)
sources = $(wildcard *.lua) $(wildcard lib/*)

package = paint_world.love
win_exe = $(package:%.love=%.exe)
win_zip = $(package:%.love=%_win.zip)
osx_app = $(package:%.love=%.app)
osx_zip = $(package:%.love=%.app.zip)

all: $(package)

$(package): $(sources) $(assets)
	zip -qr $(package) $(sources) $(assets)

$(win_zip): $(package)
	cat build-deps/love.exe $(package) > $(win_exe)
	zip -qj $(win_zip) $(win_exe) build-deps/*.dll build-deps/license.txt
	rm -f $(win_exe)

$(osx_zip): $(package)
	cp -r build-deps/love.app $(osx_app)
	cp $(package) $(osx_app)/Contents/Resources/
	cp build-deps/Info.plist $(osx_app)/Contents/Info.plist
	zip -qry $(osx_zip) $(osx_app)

clean:
	rm -rf $(package) $(win_exe) $(win_zip) $(osx_app) $(osx_zip)

.PHONY: all clean
