local Class = require "lib.hump.class"

local world_shader = require "world_shader"

local World = Class{
  init = function(self, filename)
    self.bg = love.graphics.newImage(filename)
    self.bg:setFilter("linear", "nearest")
    self.bgData = self.bg:getData()

    self.materials = love.graphics.newImage("art/material0.png")
    self.materials:setFilter("linear", "nearest")

    self.shader = love.graphics.newShader(world_shader)
    self.shader:send("materials", self.materials)
    self.shader:send("world_size", 256)
    self.shader:send("material_size", 16)
  end,
}

function World:draw()
  love.graphics.setShader(self.shader)
  love.graphics.draw(self.bg)
  love.graphics.setShader()
end

return World
