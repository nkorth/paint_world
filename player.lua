local Class = require "lib.hump.class"

local Player = Class{
  init = function(self, x, y, filename)
    self.x, self.y = x, y
    self.drawX, self.drawY = x, y
    self.vx, self.vy = 0, 0
    self.facing_right = true

    self.alive = true
    self.danger_time = 0.5

    self.can_jump = false
    self.jump_timer = 0
    self.jump_was_down = false

    self.attack_was_down = false

    self.walk_anim = 0
    self.flip_anim = 0
    self.pulse_anim = 0
    self.ring_anim = 0

    self.image = love.graphics.newImage(filename)
  end,
}

function Player:is_touching(other_player)
  local dx = other_player.x - player.x
  local dy = other_player.y - player.y
  return math.abs(dx) < 16 and math.abs(dy) < 16
end

local function _pointIsSolid(worldData, x, y)
  if x < 0 or y < 0 or x >= worldData:getWidth() or y >= worldData:getHeight() then
    return true
  end
  r, g, b, a = worldData:getPixel(math.floor(x), math.floor(y))
  return r ~= 0
end

function Player:move(dx, dy, worldData)
  self:_move_x(dx, worldData)
  self:_move_y(dy, worldData)
end

function Player:_move_x(dx, worldData)
  if dx == 0 then return true end

  while math.abs(dx) > 1 do
    -- move one pixel at a time
    if dx > 0 then
      if not self:_move_x(1, worldData) then return false end
      dx = dx - 1
    elseif dx < 0 then
      if not self:_move_x(-1, worldData) then return false end
      dx = dx + 1
    end
  end

  if not _pointIsSolid(worldData, self.x + dx, self.y) then
    self.x = self.x + dx
    return true
  elseif self.can_jump and not _pointIsSolid(worldData, self.x + dx, self.y - 1) then
    -- climb shallow slopes
    self.x = self.x + dx
    self.y = self.y - 1
    return true
  else
    return false
  end
end

function Player:_move_y(dy, worldData)
  if dy == 0 then return true end

  while math.abs(dy) > 1 do
    -- move one pixel at a time
    if dy > 0 then
      if not self:_move_y(1, worldData) then return false end
      dy = dy - 1
    elseif dy < 0 then
      if not self:_move_y(-1, worldData) then return false end
      dy = dy + 1
    end
  end

  if not _pointIsSolid(worldData, self.x, self.y + dy) then
    self.y = self.y + dy
    return true
  else
    -- hit the ground
    self.can_jump = true
    self.vy = 0
    return false
  end
end

function Player:control(dt, world)
  if love.keyboard.isDown("space") then
    if self.can_jump and not self.jump_was_down then
      self.can_jump = false
      self.jump_was_down = true
      self.jump_timer = 1
    end
    if self.jump_timer > 0 then
      self.jump_timer = math.max(0, self.jump_timer - 5*dt)
      self.vy = -200
    end
  else
    self.jump_was_down = false
    self.jump_timer = 0
  end

  if self.danger_time < 4.9 then -- lose control for a moment of knockback
    if love.keyboard.isDown("a") then
      self.vx = -100
      self.facing_right = false
    elseif love.keyboard.isDown("d") then
      self.vx = 100
      self.facing_right = true
    else
      self.vx = 0
    end
  end

  local dx, dy = self.vx*dt, self.vy*dt
  self:move(dx, dy, world.bgData)
end

function Player:auto_wander(dt, world)
  if self.facing_right then
    self.vx = 30
  else
    self.vx = -30
  end

  local dx, dy = self.vx*dt, self.vy*dt
  local collided = not self:_move_x(dx, world.bgData)
  self:_move_y(dy, world.bgData)

  if collided then
    self.facing_right = not self.facing_right
  end
end

function Player:take_damage(other_player)
  -- give the player a couple seconds of invincibility
  if self.danger_time > 3 then return end

  if self.danger_time > 0 then
    error("YOU DIED")
  else
    -- knockback
    if other_player.x > player.x then
      self.vx = -300
    else
      self.vx = 300
    end
    -- activate danger
    self.ring_anim = -1
    self.danger_time = 5
  end
end

function Player:update(dt)
  -- enforce gravity and terminal velocity
  self.vy = math.min(self.vy + 1000*dt, 500)

  -- walk animation
  if self.vx ~= 0 and self.can_jump then
    self.walk_anim = (self.walk_anim + 3*dt) % 1
  else
    self.walk_anim = 0
  end

  -- flip animation
  local flip_speed = 10
  if self.facing_right then
    self.flip_anim = math.min(1, self.flip_anim + flip_speed*dt)
  else
    self.flip_anim = math.max(-1, self.flip_anim - flip_speed*dt)
  end

  -- danger cooldown and red pulse animation
  if self.danger_time > 0 then
    self.pulse_anim = (self.pulse_anim + 2*dt) % 1
    self.danger_time = math.max(0, self.danger_time - dt)
    if self.danger_time == 0 then
      -- play recover animation
      self.ring_anim = 1
    end
  else
    self.pulse_anim = 0
  end

  -- hurt/recover ring animation
  if self.ring_anim > 0 then
    self.ring_anim = math.max(0, self.ring_anim - dt)
  elseif self.ring_anim < 0 then
    self.ring_anim = math.min(0, self.ring_anim + dt)
  end

  -- the "drawPos" is smoothed out from the actual position
  self.drawX = self.drawX + (self.x - self.drawX)*dt*30
  self.drawY = self.drawY + (self.y - self.drawY)*dt*30
end

function Player:draw()
  local pulse = math.abs(self.pulse_anim - 0.5) * 2
  love.graphics.setColor(255, pulse*255, pulse*255)

  local x, y = self.drawX, self.drawY

  local r = 0
  local walk_frame = math.floor(self.walk_anim * 4)
  if walk_frame == 1 then
      r = math.pi/20
  elseif walk_frame == 3 then
      r = -math.pi/20
  end

  local sx, sy = 0.25*self.flip_anim, 0.25

  love.graphics.draw(self.image, x, y-7, r, sx, sy, 32, 96)

  -- hurt/recover animation
  love.graphics.setLineWidth(1)
  local t = 1 - 16*math.pow(0.5 - math.abs(self.ring_anim), 4)
  if self.ring_anim > 0 then
    if self.ring_anim > 0.5 then
      for i = 0,2 do
        local r = 20 + math.max(0, (1 - t - i*0.2) * 20)
        love.graphics.setColor(100, 100, 255)
        love.graphics.circle("line", x, y-16, r, 32)
      end
    else
      love.graphics.setColor(100, 100, 255, t * 255)
      love.graphics.circle("line", x, y-16, t * 20, 32)
    end
  elseif self.ring_anim < 0 then
    if self.ring_anim < -0.5 then
      love.graphics.setColor(100, 100, 255)
      love.graphics.circle("line", x, y-16, 20, 32)
    else
      love.graphics.setColor(100, 100, 255, t * 255)
      --local r = 20 + (self.ring_anim + 0.5) * 4000
      local r = 20 + (1-t) * 400
      love.graphics.circle("line", x, y-16, r, 32)
    end
  end
end

return Player
